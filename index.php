<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Ejercicio 4</title>
</head>
<body>
	<?php 
		$var = 24.5;
		$tipo1 = gettype($var);
		echo nl2br("$tipo1\n");
		$var = 'HOLA';
		$tipo2 = gettype($var);
		echo nl2br("$tipo2\n");
		$var = 10;
		var_dump($var);
	 ?>
</body>
</html>